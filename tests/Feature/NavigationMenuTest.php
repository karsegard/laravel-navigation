<?php

namespace KDA\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use KDA\Navigation\Models\Navigation;
use KDA\Navigation\Models\Menu;

use KDA\Tests\TestCase;

class NavigationMenuTest extends TestCase
{
  use RefreshDatabase;
 
  /** @test */

  function a_menu_has_navigations()
  {
    $o = Menu::factory()->create(['name' => 'Fake Title']);;
    Navigation::factory()->create(['name'=>'Fake Navigation']);
    $n = Navigation::factory()->create(['name'=>'Fake Navigation','menu_id'=>$o->id]);
    $this->assertEquals('Fake Title', $o->name);

    $this->assertEquals(1,$o->navigations->count());


  }

  /** @test */

  function a_menu_has_navigations_2()
  {
    $o = Menu::factory()->create(['name' => 'Fake Title']);;
    Navigation::factory()->create(['name'=>'Fake Navigation']);
    $n = Navigation::factory()->create(['name'=>'Fake Navigation','menu_id'=>$o->id]);
    $n = Navigation::factory()->create(['name'=>'Fake Navigation','menu_id'=>$o->id]);
    $n = Navigation::factory()->create(['name'=>'Fake Navigation','menu_id'=>$o->id]);
    $this->assertEquals('Fake Title', $o->name);

    $this->assertEquals(3,$o->navigations->count());


  }
  

   /** @test */

   function a_menu_has_navigations_3()
   {
     $o = Menu::factory()->create(['name' => 'Fake Title']);;
     $o2 = Menu::factory()->create(['name' => 'Fake Title 2']);;
     Navigation::factory()->create(['name'=>'Fake Navigation']);
     $n = Navigation::factory()->create(['name'=>'Fake Navigation','menu_id'=>$o->id]);
     $n = Navigation::factory()->create(['name'=>'Fake Navigation','menu_id'=>$o->id]);
     $n = Navigation::factory()->create(['name'=>'Fake Navigation','menu_id'=>$o->id]);
     $n = Navigation::factory()->create(['name'=>'Fake Navigation','menu_id'=>$o2->id]);
     $this->assertEquals('Fake Title', $o->name);
 
     $this->assertEquals(3,$o->navigations->count());
     $this->assertEquals(1,$o2->navigations->count());
 
 
   }
  
   /** @test */

   function navigation_are_retrievable_by_menu_key()
   {
     $o = Menu::factory()->create(['name' => 'Top']);
     $o2 = Menu::factory()->create(['name' => 'Bottom']);


     Navigation::factory()->create(['name'=>'Fake Navigation']);
     Navigation::factory()->create(['name'=>'Fake Navigation','menu_id'=>$o->id]);
     Navigation::factory()->create(['name'=>'Fake Navigation','menu_id'=>$o->id]);
     Navigation::factory()->create(['name'=>'Fake Navigation','menu_id'=>$o->id]);
     Navigation::factory()->create(['name'=>'Fake Navigation','menu_id'=>$o2->id]);
     


    $navs = Navigation::forMenu('top');
    
    $this->assertEquals(3,$navs->get()->count());
 
   }
}