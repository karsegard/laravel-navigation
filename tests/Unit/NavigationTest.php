<?php

namespace KDA\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use KDA\Navigation\Models\Navigation;

use KDA\Tests\TestCase;

class NavigationTest extends TestCase
{
  use RefreshDatabase;
  /** @test */
  function a_navigation_has_a_name()
  {
    $o = Navigation::factory()->create(['name' => 'Fake Title']);
    $this->assertEquals('Fake Title', $o->name);
    $this->assertEquals(false, $o->display_in_footer);
    $this->assertEquals(false, $o->display_in_header);

  }

    /** @test */
  function a_navigation_has_a_parent()
  {
    $o = Navigation::factory()->create(['name'=>'']);

    $o2 = Navigation::factory()->create(['name'=>'','parent_id'=>$o]);

    $this->assertEquals($o->id, $o2->parent_id);
  }

 
  
}