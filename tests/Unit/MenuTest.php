<?php

namespace KDA\Tests\Unit;

use KDA\Navigation\Models\Navigation;
use Illuminate\Foundation\Testing\RefreshDatabase;
use KDA\Navigation\Models\Menu;

use KDA\Tests\TestCase;

class MenuTest extends TestCase
{
  use RefreshDatabase;


  /** @test */

  function ensure_we_cant_get_duplicates()
  {
    $this->expectException(\Illuminate\Database\QueryException::class);

    Menu::factory()->create(['name' => 'Fake Title']);;
    Menu::factory()->create(['name' => 'Fake Title']);;

  }

  /** @test */
  function a_menu_has_a_name()
  {
    $o = Menu::factory()->create(['name' => 'Fake Title']);
    $this->assertEquals('Fake Title', $o->name);

  }
  /** @test */
  function a_menu_has_a_default_key()
  {
    $o = Menu::factory()->create(['name' => 'Fake Title']);
    $this->assertEquals('fake-title', $o->key);

  }


}