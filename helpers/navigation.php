<?php

use KDA\Navigation\Models\Navigation;

if (!function_exists('navigation')) {
    function navigation($key)
    {
        return Navigation::forMenuKey($key)->root()->get();
    }
}


