<?php

namespace KDA\Navigation;

use KDA\Laravel\PackageServiceProvider;

class ServiceProvider extends PackageServiceProvider
{


    use \KDA\Laravel\Traits\HasLoadableMigration;
    use \KDA\Laravel\Traits\HasDumps;
    use \KDA\Laravel\Traits\HasHelper;

  //  use \KDA\Laravel\Traits\ProvidesBlueprint;
    protected $dumps=[
        'navigations',
        'menus'
    ];

   

    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }


}
