<?php

namespace KDA\Navigation\View\Composers;

use Illuminate\View\View;
use KDA\Navigation\Models\Navigation;


class NavigationComposer
{
    

    /**
     * Bind data to the view.
     *
     * @param  \Illuminate\View\View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with(
            ['nav'=>Navigation::getTree()]
            
            
        );


    }
}