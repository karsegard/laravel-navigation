<?php

namespace KDA\Navigation\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Str;
class Menu extends Model
{
    use HasFactory, SoftDeletes;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'key',
        'max_items',
        'max_depth'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
       
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model)
        {
           $model->setDefaultKey();
        });
    }
    
    public function navigations(){
        return $this->hasMany(Navigation::class);
    }

    public function setDefaultKey(){
        if(empty($this->attributes['key'])){
            $this->attributes['key']= Str::slug($this->attributes['name']);
        }
    }

    protected static function newFactory()
    {
        return  \KDA\Navigation\Database\Factories\MenuFactory::new();
    }
}
