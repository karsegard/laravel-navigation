<?php

namespace KDA\Navigation\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Navigation extends Model
{
    use HasFactory, SoftDeletes;
    use \KDA\Laravel\Models\Traits\Nested;

    protected $parentAttribute = 'parent_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'url',
        'external_url',
        'navigable_type',
        'navigable_id',
        'route',
        'parent_id',
        'lft',
        'rgt',
        'depth',
        'menu_id'
        
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'parent_id' => 'integer',
        'menu_id' => 'integer',
        'lft' => 'integer',
        'rgt' => 'integer',
        'depth' => 'integer',
    ];

    protected $appends = [
        'navigation_url',
        'is_currently_browsed'
    ];

    public function navigation()
    {
        return $this->hasOne(Navigation::class);
    }

    public function parent()
    {
        return $this->belongsTo(Navigation::class);
    }

    public function navigable()
    {
        return $this->morphTo();
    }


    public function menu()
    {
        return $this->belongsTo(Menu::class);
    }

    protected static function newFactory()
    {
        return  \KDA\Navigation\Database\Factories\NavigationFactory::new();
    }
    public function identifiableAttribute()
    {
        return 'name';
    }

    public function getIsNavigableAttribute(){
        return $this->url || $this->external_url || $this->route || $this->navigable; 
    }


    public function getNavigationUrlAttribute(){
        $url = '#';
        if($this->navigable){
            $url = $this->navigable->url;
        }elseif (!empty($this->route)) {
            $url = route($this->route,[],false);
           
        } elseif (!empty($this->url)) {
            $value = $this->url;
            if(strpos($value,'/')!==0){
                $value = '/'.$value;
            }
            $url = $value;
            
        } elseif (!empty($this->external_url)) {
            $url = $this->external_url;
            
        }
        return $url;
    }

    public function getIsCurrentlyBrowsedAttribute(){
        $active = false;
        if (!empty($this->route)) {
            $active = request()->route()->named($this->route);
        } else  {
            $active = preg_match('/' . str_replace('/', '\/', $this->navigation_url) . '.*/', \URL::current()) >0;
            
        }

        return $active;
    }
/*
    public function setUrlAttribute($value){
        if(strpos($value,'/')!==0){
            $value = '/'.$value;
        }
        $this->attributes['url']=$value;
    }*/
    public function scopeForMenuKey($q,$menu){
        return $q->whereHas('menu',function($q) use ($menu){
            $q->where('key',$menu);
        });
    }
    public function scopeForMenu($q,$menu_id){
        return $q->where('menu_id',$menu_id);
    }

    public function scopeRoot($q){
        return $q->whereNull('parent_id')->orderBy('lft');
    }
}
