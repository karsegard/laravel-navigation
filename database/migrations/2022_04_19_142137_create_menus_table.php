<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::create('menus', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('key')->unique();
            $table->integer('max_depth')->default(1);
            $table->integer('max_items')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });



        Schema::table('navigations', function (Blueprint $table) {
            $table->foreignId('menu_id')->nullable()->constrained('menus')->onDelete('cascade');
            $table->dropColumn('display_in_header');
            $table->dropColumn('display_in_footer');
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('navigations', function (Blueprint $table) {
            $table->dropForeign(['menu_id']);
            $table->dropColumn('menu_id');
            $table->boolean('display_in_footer')->default(1);
            $table->boolean('display_in_header')->default(1);
        });

        Schema::dropIfExists('menus');
        Schema::enableForeignKeyConstraints();
    }
}
