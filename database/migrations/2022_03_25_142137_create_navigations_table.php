<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNavigationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::create('navigations', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            
            $table->nullableMorphs('navigable');
            
            $table->string('url')->nullable();
            $table->string('external_url')->nullable();
            $table->string('route')->nullable();

            $table->boolean('display_in_footer')->default(1);
            $table->boolean('display_in_header')->default(1);
            $table->nestable();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();

        Schema::dropIfExists('navigations');
        Schema::enableForeignKeyConstraints();

    }
}
