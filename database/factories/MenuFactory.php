<?php

namespace KDA\Navigation\Database\Factories;

use KDA\Navigation\Models\Menu;
use Illuminate\Database\Eloquent\Factories\Factory;

class MenuFactory extends Factory
{
    protected $model = Menu::class;

    public function definition()
    {
        return [
            //
        ];
    }
}
