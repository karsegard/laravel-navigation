<?php

namespace KDA\Navigation\Database\Factories;

use KDA\Navigation\Models\Navigation;
use Illuminate\Database\Eloquent\Factories\Factory;

class NavigationFactory extends Factory
{
    protected $model = Navigation::class;

    public function definition()
    {
        return [
            //
        ];
    }
}
